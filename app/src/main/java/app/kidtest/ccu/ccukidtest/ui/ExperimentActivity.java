package app.kidtest.ccu.ccukidtest.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import app.kidtest.ccu.ccukidtest.Application.CCUTestApplication;
import app.kidtest.ccu.ccukidtest.R;
import app.kidtest.ccu.ccukidtest.database.SQLiteDBControl;
import app.kidtest.ccu.ccukidtest.errorHandling.ErrorHandle;
import app.kidtest.ccu.ccukidtest.object.Student;
import app.kidtest.ccu.ccukidtest.object.Test;
import app.kidtest.ccu.ccukidtest.utility.ExcelGenerate;


public class ExperimentActivity extends AppCompatActivity implements View.OnClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener{
    private int exampleDoingCount, testDoingCount;// count the question's of example and test
    private int correctCount;
    private boolean checkingAnswer;//uses to lock system when system is checking answer
    private ImageButton expImgBtn1, expImgBtn2, expImgBtn3, expImgBtn4;
    private ImageView expImgFocus1, expImgFocus2;
    private FloatingActionButton expFloatBtnPause, expFloatBtnReplay;
    private TextView expTvCount;
    private int[] answerArray;
    private int[] exampleAnswerArray = {4,2,1};
    private JSONArray testResultJsonArray = new JSONArray();
    private boolean isSoundFinished, isPaused;// lock action when is playing music or pausing
    public static final String BUNDLE_STUDENT="BUNDLE_STUDENT";
    private Student student=null;
    private long countAnswerTime, questionStartTime;//count the answering time
    private long totalAnswerTime;// to mesure the total time cost;
    MediaPlayer media ;
    Handler myHandler = new Handler();;
    String packageName ;
    boolean giveResponse;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
   // private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment);
        setUI();
        initialValue();
        Bundle bundle = this.getIntent().getExtras();
        ExcelGenerate excelGenerateTest = new ExcelGenerate();//detect answer excel file and load it into array
        try {
            answerArray = excelGenerateTest.questionAnswerDetect(ExperimentActivity.this);
            JSONObject jsonStudent = new JSONObject(bundle.getString(BUNDLE_STUDENT));// get student by user setting
            this.student = new Student(jsonStudent);
        }catch (Exception e){
            e.printStackTrace();
        }
        nextQuestion(true, exampleDoingCount);// doing first example question
    }

    protected void initialValue(){
        correctCount=0;//correct answer count for caculating the result points
        exampleDoingCount = 1;//example question count, init=1
        testDoingCount = 1;// test question count, init=1
        checkingAnswer=false;//
        isPaused=false;
        isSoundFinished=true;
        giveResponse = false;
        packageName = ExperimentActivity.this.getPackageName();
    }
    private void setUI(){
        expImgBtn1 = (ImageButton)findViewById(R.id.experiment_imgBtn1);
        expImgBtn2 = (ImageButton)findViewById(R.id.experiment_imgBtn2);
        expImgBtn3 = (ImageButton)findViewById(R.id.experiment_imgBtn3);
        expImgBtn4 = (ImageButton)findViewById(R.id.experiment_imgBtn4);
        expImgBtn1.setOnClickListener(this);
        expImgBtn2.setOnClickListener(this);
        expImgBtn3.setOnClickListener(this);
        expImgBtn4.setOnClickListener(this);
        expFloatBtnPause = (FloatingActionButton)findViewById(R.id.experiment_fbtn_pause);
        expFloatBtnReplay = (FloatingActionButton)findViewById(R.id.experiment_fbtn_replay);
        expFloatBtnPause.setOnClickListener(this);
        expFloatBtnReplay.setOnClickListener(this);
        expTvCount = (TextView)findViewById(R.id.experiment_tv_count);
        expTvCount.setVisibility(View.INVISIBLE);
        expImgFocus1 = (ImageView)findViewById(R.id.experiment_cross1);
        expImgFocus2 = (ImageView)findViewById(R.id.experiment_cross2);
    }

    private void nextQuestion(boolean isExample, int doingCount) {
        String tempPath = ":drawable/";
        String questionStartChar;
        String testFilePath;
        File sdcard = Environment.getExternalStorageDirectory();
        String mediaPath;
        String mediaEndStr;
        String imgFormat;
        String selectionStartChar = "p";
        File imgFile;
        Bitmap imgBitmap;
        if (isExample) { //load default data
            questionStartChar = "pra";
            testFilePath = packageName + tempPath;
            imgFormat = "";
            int imgId;
            imgId = getResources().getIdentifier(packageName + ":drawable/" + questionStartChar + doingCount + selectionStartChar + 1, null, null);
            expImgBtn1.setImageResource(imgId);
            imgId = getResources().getIdentifier(packageName + ":drawable/" + questionStartChar + doingCount + selectionStartChar + 2, null, null);
            expImgBtn2.setImageResource(imgId);
            imgId = getResources().getIdentifier(packageName + ":drawable/" + questionStartChar + doingCount + selectionStartChar + 3, null, null);
            expImgBtn3.setImageResource(imgId);
            imgId = getResources().getIdentifier(packageName + ":drawable/" + questionStartChar + doingCount + selectionStartChar + 4, null, null);
            expImgBtn4.setImageResource(imgId);
            mediaEndStr = "s";
            int mediaId;
            mediaId = getResources().getIdentifier(packageName + ":raw/" + questionStartChar + doingCount + mediaEndStr, null, null);
            media = MediaPlayer.create(this, mediaId);
            Log.d(this.getClass().getName(), "img id:" + imgId);
        } else { // load external data
            expTvCount.setVisibility(View.VISIBLE);
            questionStartChar = "t";
            imgFormat = ".jpg";
            mediaEndStr = "s.mp3";
            testFilePath = CCUTestApplication.dirName + File.separator + CCUTestApplication.testDirName + File.separator;
            imgFile = new File(sdcard, testFilePath + questionStartChar + doingCount + selectionStartChar + 1 + imgFormat);
            imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            expImgBtn1.setImageBitmap(imgBitmap);
            imgFile = new File(sdcard, testFilePath + questionStartChar + doingCount + selectionStartChar + 2 + imgFormat);
            imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            expImgBtn2.setImageBitmap(imgBitmap);
            imgFile = new File(sdcard, testFilePath + questionStartChar + doingCount + selectionStartChar + 3 + imgFormat);
            imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            expImgBtn3.setImageBitmap(imgBitmap);
            imgFile = new File(sdcard, testFilePath + questionStartChar + doingCount + selectionStartChar + 4 + imgFormat);
            imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            expImgBtn4.setImageBitmap(imgBitmap);
            expTvCount.setText(String.valueOf(doingCount));
            media = MediaPlayer.create(this, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + testFilePath + questionStartChar + doingCount + mediaEndStr));
            Log.d(this.getClass().getName(), imgFile.getAbsolutePath());
        }
        try {
            //media = MediaPlayer.create(this, raw1);
            //Log.d(this.getClass().getName(), "Media Path:" + Environment.getExternalStorageDirectory() + File.separator + testFilePath + questionStartChar + doingCount + mediaEndStr);
            media.setOnCompletionListener(this);
            media.setOnErrorListener(this);
            this.isSoundFinished = false;
            //Log.d(this.getClass().getName(), "Media Duration:" + media.getDuration());
            media.start();
            countAnswerTime = 0;
            questionStartTime = System.currentTimeMillis();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_FILE_NOTFOUND);
        }

    }

    @Override
    public void onClick(View v) {
        try{
                switch (v.getId()) {
                    case R.id.experiment_imgBtn1:
                        checkAnswer(1);
                        break;
                    case R.id.experiment_imgBtn2:
                        checkAnswer(2);
                        break;
                    case R.id.experiment_imgBtn3:
                        checkAnswer(3);
                        break;
                    case R.id.experiment_imgBtn4:
                        checkAnswer(4);
                        break;
                    case R.id.experiment_fbtn_pause:
                        if (isPaused) {
                            Snackbar.make(v, "測驗繼續", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            questionStartTime = System.currentTimeMillis();
                            isPaused = false;
                            expFloatBtnPause.setImageResource(android.R.drawable.ic_media_pause);
                        } else {
                            if (isSoundFinished) { // only can pause when sound is finish;
                                isPaused = true;
                                Snackbar.make(v, "測驗暫停", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                                Long timeUsed = System.currentTimeMillis() - questionStartTime;
                                countAnswerTime += timeUsed;
                                expFloatBtnPause.setImageResource(android.R.drawable.ic_media_play);
                            }
                        }
                        break;
                    case R.id.experiment_fbtn_replay:
                        if (isPaused) {
                            Toast.makeText(this, "請先按下右下角三角形繼續作答", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (isSoundFinished) {
                            Snackbar.make(v, "重新播放", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            isSoundFinished = false;
                            Long timeUsed = System.currentTimeMillis() - questionStartTime;
                            countAnswerTime += timeUsed;
                            media.start();
                        }
                        break;
                }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void checkAnswer(final int select){
        Log.d(this.getClass().getName(), "Example count:"+exampleDoingCount + " Test count:"+ testDoingCount + " Answer:" + select + "true answer:" + answerArray[testDoingCount-1]);
        if(isPaused) {
            Toast.makeText(this, "請先按下右下角三角形繼續作答", Toast.LENGTH_LONG).show();
            return;
        }
        if(isSoundFinished && !checkingAnswer) {
            try {
                Thread.sleep(300);
            }catch(Exception e){
                e.printStackTrace();
            }
            expImgBtn1.setClickable(false);
            expImgBtn2.setClickable(false);
            expImgBtn3.setClickable(false);
            expImgBtn4.setClickable(false);
            checkingAnswer = true;
            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.sleep(1500);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    return null;
                }
                @Override
                protected void onPreExecute() {
                    expImgBtn1.setVisibility(View.INVISIBLE);
                    expImgBtn2.setVisibility(View.INVISIBLE);
                    expImgBtn3.setVisibility(View.INVISIBLE);
                    expImgBtn4.setVisibility(View.INVISIBLE);
                    expImgFocus1.setVisibility(View.VISIBLE);
                    expImgFocus2.setVisibility(View.VISIBLE);
                    if (exampleDoingCount <= 3) {
                            startCheckExampleAnswer(select);
                    }else if(testDoingCount<=answerArray.length){
                        try {
                            startCheckAnswer(select);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    media.release();
                    if (exampleDoingCount < 3) {
                        exampleDoingCount++;
                        nextQuestion(true, exampleDoingCount);
                    }else if(exampleDoingCount == 3){
                        exampleDoingCount++;
                        expImgBtn1.setVisibility(View.INVISIBLE);
                        expImgBtn2.setVisibility(View.INVISIBLE);
                        expImgBtn3.setVisibility(View.INVISIBLE);
                        expImgBtn4.setVisibility(View.INVISIBLE);
                        Dialog dialog = new StartTestDialog(ExperimentActivity.this);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Dialog startTestDialog = new DynamicDialog(ExperimentActivity.this, "按下一步，測驗正式開始", "下一步");
                                startTestDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        nextQuestion(false, testDoingCount);
                                    }
                                });
                                startTestDialog.show();
                            }
                        });
                        dialog.show();
                    }else if(testDoingCount<=answerArray.length && giveResponse){
                        giveAnswerCorrectResponse(testDoingCount);
                    }else if(testDoingCount<=answerArray.length){
                        testNextQuestion();
                    }
                    expImgBtn1.setVisibility(View.VISIBLE);
                    expImgBtn2.setVisibility(View.VISIBLE);
                    expImgBtn3.setVisibility(View.VISIBLE);
                    expImgBtn4.setVisibility(View.VISIBLE);
                    expImgBtn1.setClickable(true);
                    expImgBtn2.setClickable(true);
                    expImgBtn3.setClickable(true);
                    expImgBtn4.setClickable(true);
                    expImgFocus1.setVisibility(View.INVISIBLE);
                    expImgFocus2.setVisibility(View.INVISIBLE);
                    checkingAnswer = false;


                }
            }.execute();
        }else {
            return ;
        }

    }
    //example use
    private void startCheckExampleAnswer(int select){
        if(select==exampleAnswerArray[exampleDoingCount-1]){
            Toast.makeText(this, "答對了！", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "答錯了！", Toast.LENGTH_SHORT).show();
        }
    }
    private void startCheckAnswer(int select) throws JSONException{
                Test test ;
                Long answeredTime = System.currentTimeMillis() - questionStartTime;
                countAnswerTime += answeredTime;
                totalAnswerTime +=countAnswerTime;
                Log.d(this.getClass().getName(), "Answer time cost:" + countAnswerTime);
                if(select==answerArray[testDoingCount-1]){
                    Toast.makeText(this, "答對了！", Toast.LENGTH_SHORT).show();
                    giveResponse = checkGiveCorrectResponse(testDoingCount);
                    test = new Test(String.valueOf(testDoingCount), select, true, countAnswerTime);
                    correctCount++;
                }else{
                    Toast.makeText(this, "答錯了！", Toast.LENGTH_SHORT).show();
                    giveResponse=false;
                    test = new Test(String.valueOf(testDoingCount), select, false, countAnswerTime);
                }
                testResultJsonArray.put(testDoingCount-1, Test.testToJSON(test));// JsonArray start from index 0
    }

    private void testNextQuestion(){
        if(testDoingCount<answerArray.length) {
            testDoingCount++;
            nextQuestion(false, testDoingCount);
        }else if(testDoingCount==answerArray.length){
                new AsyncTask<Void, Void, Void>(){
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        expImgBtn1.setVisibility(View.INVISIBLE);
                        expImgBtn2.setVisibility(View.INVISIBLE);
                        expImgBtn3.setVisibility(View.INVISIBLE);
                        expImgBtn4.setVisibility(View.INVISIBLE);
                        expImgFocus1.setVisibility(View.INVISIBLE);
                        expImgFocus2.setVisibility(View.INVISIBLE);
                        Toast.makeText(ExperimentActivity.this, "測驗結束！", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Bundle bundle = new Bundle();
                        bundle.putString(ResultActivity.RESULT_POINT, String.valueOf(correctCount*2));// each question 2 points
                        Intent intent = new Intent();
                        intent.setClass(ExperimentActivity.this, ResultActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        ExperimentActivity.this.finish();
                    }

                    @Override
                    protected Void doInBackground(Void... params) {
                        testDoingCount++;
                        student.setStuTestResult(testResultJsonArray);
                        student.setCorrectCount(correctCount);
                        student.setQuestionCount(answerArray.length);
                        student.setStuAvgAnswerTime(totalAnswerTime / answerArray.length);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy MMMM d");
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        String testDate = format.format(calendar.getTime());
                        student.setTestDate(testDate);
                        handleTestResult(student);
                        return null;
                    }


                }.execute();


        }
    }



    @Override
    public void onCompletion(MediaPlayer mp) {
        this.isSoundFinished = true;
        questionStartTime=System.currentTimeMillis();
        Log.d(this.getClass().getName(), "Media onCompleted");
        // mp.release();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    private void handleTestResult(Student student){
        SQLiteDBControl myDb = SQLiteDBControl.getInstance(this);
        myDb.open();
        //myDb.cleanDb();
        Log.d(this.getClass().getName(), student.studentToJSON().toString());
        Student insetStudent = myDb.addStudent(student);
        Log.d(this.getClass().getName(), insetStudent.studentToJSON().toString());

        List<Student> studentList = myDb.queryStudent();
        Log.d(this.getClass().getName(), "size:"+studentList.size());
        if(studentList.size()>0) {
            for(int i=0; i<studentList.size(); i++){
                Log.d(this.getClass().getName(), "Student:"+ studentList.get(i).studentToJSON().toString());
            }
        }
        myDb.close();
    }

    private void changeQuestionDelay(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(1000);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPreExecute() {
                expImgBtn1.setVisibility(View.INVISIBLE);
                expImgBtn2.setVisibility(View.INVISIBLE);
                expImgBtn3.setVisibility(View.INVISIBLE);
                expImgBtn4.setVisibility(View.INVISIBLE);
                expImgFocus1.setVisibility(View.VISIBLE);
                expImgFocus2.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                expImgBtn1.setVisibility(View.VISIBLE);
                expImgBtn2.setVisibility(View.VISIBLE);
                expImgBtn3.setVisibility(View.VISIBLE);
                expImgBtn4.setVisibility(View.VISIBLE);
                expImgFocus1.setVisibility(View.INVISIBLE);
                expImgFocus2.setVisibility(View.INVISIBLE);
            }
        }.execute();
    }
    private boolean checkGiveCorrectResponse(int questionNumber){
        if(questionNumber==1 || questionNumber == 21 || questionNumber==10 || questionNumber == 36 || questionNumber == 45){
            return true;
        }else
            return false;
    }
    private void giveAnswerCorrectResponse(int questionNumber){
        if(questionNumber==1 || questionNumber == 21 ){
            final Dialog responseDialog;
            responseDialog = new FirstResponseDialog(this);
            responseDialog.show();
            int introRawId = getResources().getIdentifier(getPackageName() + ":raw/clap1s", null, null);
            try {
                media = MediaPlayer.create(this, introRawId);
                media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.d(this.getClass().getName(), "Response Dialog media completed");
                        mp.stop();
                        responseDialog.dismiss();
                        testNextQuestion();
                    }
                });
                media.start();
            }catch(Exception e){
                e.printStackTrace();
            }
        }else if( questionNumber==10 || questionNumber == 36 || questionNumber == 45){
            final Dialog responseDialog;
            responseDialog = new ResponseDialog(this);
            responseDialog.show();
            int introRawId = getResources().getIdentifier(getPackageName() + ":raw/clap1s", null, null);
            try {
                media = MediaPlayer.create(this, introRawId);
                media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.d(this.getClass().getName(), "Response Dialog media completed");
                        mp.stop();
                        responseDialog.dismiss();
                        testNextQuestion();
                    }
                });
                media.start();
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            return ;
        }
        return ;
    }

    public class FirstResponseDialog extends Dialog {
        ImageView iv;
        public FirstResponseDialog(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setCanceledOnTouchOutside(false);
            setContentView(R.layout.dialog_imagelayout);
            iv = (ImageView)findViewById(R.id.introDialog_iv);
            iv.setImageResource(R.drawable.fbp1);

        }
    }

    public class ResponseDialog extends Dialog {
        ImageView iv;
        public ResponseDialog(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_imagelayout);
            setCanceledOnTouchOutside(false);
            iv = (ImageView)findViewById(R.id.introDialog_iv);
            iv.setImageResource(R.drawable.fbp2p1);

            myHandler = new Handler();
            myHandler.postDelayed(mMyRunnable, 500);
        }
        Handler myHandler;
        boolean switchBoolean= true;
        private Runnable mMyRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                //Change state here
                if(switchBoolean){
                    iv.setImageResource(R.drawable.fbp2p2);
                    switchBoolean=false;
                    myHandler.postDelayed(this, 500);
                }else{
                    iv.setImageResource(R.drawable.fbp2p1);
                    switchBoolean=true;
                    myHandler.postDelayed(this, 500);
                }
            }
        };
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            new AlertDialog.Builder(ExperimentActivity.this)
                    .setTitle(R.string.experiment_exit_title)
                    .setMessage(R.string.experiment_exit_message)
                    .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (testDoingCount > 1 && testDoingCount < answerArray.length) {
                                media.release();
                                Toast.makeText(ExperimentActivity.this, "測驗結束！", Toast.LENGTH_SHORT).show();
                                try {
                                    if (media != null)
                                        media.release();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                student.setStuTestResult(testResultJsonArray);
                                student.setCorrectCount(correctCount);
                                student.setQuestionCount(testResultJsonArray.length());
                                student.setStuAvgAnswerTime(totalAnswerTime / testResultJsonArray.length());
                                SimpleDateFormat format = new SimpleDateFormat("yyyy MMMM d");
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTimeInMillis(System.currentTimeMillis());
                                String testDate = format.format(calendar.getTime());
                                student.setTestDate(testDate);
                                handleTestResult(student);
                                Bundle bundle = new Bundle();
                                bundle.putString(ResultActivity.RESULT_POINT, String.valueOf(correctCount * 2));// each question 2 points
                                Intent intent = new Intent();
                                intent.setClass(ExperimentActivity.this, ResultActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                ExperimentActivity.this.finish();
                            } else {
                                media.release();
                                Toast.makeText(ExperimentActivity.this, "測驗結束！", Toast.LENGTH_SHORT).show();
                                try {
                                    if (media != null)
                                        media.release();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ExperimentActivity.this.finish();
                            }

                        }
                    })
                    .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    }).show();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        media.release();
    }
}
