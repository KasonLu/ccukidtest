package app.kidtest.ccu.ccukidtest.ui;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import app.kidtest.ccu.ccukidtest.R;
import app.kidtest.ccu.ccukidtest.utility.MenuControl;

public class ResultActivity extends AppCompatActivity {
    TextView pointTv ;
    public static final String RESULT_POINT = "RESULT_POINT";
    MenuControl menuControl = new MenuControl(this);

    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Bundle bundle = this.getIntent().getExtras();
        String point = bundle.getString(RESULT_POINT);
        pointTv = (TextView)findViewById(R.id.result_tv_point);
        //pointTv.setText(point);
        int introRawId = getResources().getIdentifier(getPackageName() + ":raw/end1s", null, null);
        Log.d(this.getClass().getName(), "id:" + getPackageName() + ":raw/end1s");
        try {
            mediaPlayer = MediaPlayer.create(this, introRawId);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                }
            });
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.result_action_check_count:
                // User chose the "Settings" item, show the app settings UI...
                new AlertDialog.Builder(ResultActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_check_count)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(ResultActivity.this, getString(R.string.result_check_count), Toast.LENGTH_SHORT).show();
                                menuControl.showStudentCount();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            case R.id.result_action_generate_file:
                new AlertDialog.Builder(ResultActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_generate_file)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(ResultActivity.this, getString(R.string.result_generate_file), Toast.LENGTH_SHORT).show();
                                menuControl.generateFile();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            case R.id.result_action_resetdb:
                new AlertDialog.Builder(ResultActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_reset_db)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(ResultActivity.this, getString(R.string.result_reset_db), Toast.LENGTH_SHORT).show();
                                menuControl.cleanDb();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.result_items, menu);
        return true;
    }
}
