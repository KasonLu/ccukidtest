package app.kidtest.ccu.ccukidtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.kidtest.ccu.ccukidtest.R;

public class WelcomeActivity extends AppCompatActivity {
    RelativeLayout welcome_screen;
    ImageView welcome_logo;
    TextView welcome_title;
    private static final long FADE_IN_DURATION = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        welcome_screen = (RelativeLayout)findViewById(R.id.welcome_layout);
        welcome_logo = (ImageView)findViewById(R.id.welcome_iv_logo);
        welcome_title = (TextView)findViewById(R.id.welcome_tv_title);
        Animation animation = new AlphaAnimation(0,1);
        animation.setDuration(FADE_IN_DURATION);
        welcome_screen.setAnimation(animation);
//		Animation animRotateIn_icon = AnimationUtils.loadAnimation(this, R.anim.rotate);
        animation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                // TODO Auto-generated method stub
                    Intent intent = new Intent();
                    intent.setClass(WelcomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub
            }
        });
        animation.startNow();
    }
}
