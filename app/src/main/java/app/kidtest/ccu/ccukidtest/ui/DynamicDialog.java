package app.kidtest.ccu.ccukidtest.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import app.kidtest.ccu.ccukidtest.R;

/**
 * Created by kasonlu on 16/3/9.
 */
public class DynamicDialog extends Dialog{
    Button dialogBtn;
    TextView dialogTv;
    String msg,btnMsg;
    public DynamicDialog(Context context, String msg, String btnMsg) {
        super(context);
        this.msg = msg;
        this.btnMsg = btnMsg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_dynamic);
        this.setCanceledOnTouchOutside(false);
        dialogBtn = (Button)findViewById(R.id.dynamic_btn);
        dialogTv = (TextView)findViewById(R.id.dynamic_tv);
        dialogBtn.setText(btnMsg);
        dialogTv.setText(msg);
        dialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DynamicDialog.this.dismiss();
            }
        });
    }

}
