package app.kidtest.ccu.ccukidtest.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import app.kidtest.ccu.ccukidtest.R;

public class IntroActivity extends AppCompatActivity {

    private FloatingActionButton expFloatBtnPause, expFloatBtnReplay;
    private ImageView introArrow, introImage;
    Bundle bundle;
    Dialog intro1Dialog, intro2Dialog;
    MediaPlayer mediaPlayer;
    Handler imgHandler, imgHandler2, imageHandler3;
    Runnable imgRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        bundle = this.getIntent().getExtras();//put bundle from MainActivity to ExperimentActivity
        setUI();
        goIntro1();//first dialog to introduce the testing
    }

    private void setUI() {
        expFloatBtnPause = (FloatingActionButton) findViewById(R.id.experiment_fbtn_pause);
        expFloatBtnReplay = (FloatingActionButton) findViewById(R.id.experiment_fbtn_replay);
        introImage = (ImageView) findViewById(R.id.intro_iv);
        introArrow = (ImageView) findViewById(R.id.experiment_intro_arrow);
    }

    private void goIntro1() {
        int introRawId = getResources().getIdentifier(getPackageName() + ":raw/intro1s", null, null);
        Log.d(this.getClass().getName(), "id:" + getPackageName() + ":raw/intro1s");
        try {
            mediaPlayer = MediaPlayer.create(this, introRawId);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                    setPractice2();
                    imgHandler.removeCallbacks(imgRun);
                    /* comment by removing practice
                     setPractice1();// first hint of answering question
                      */
                }
            });
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgHandler = new Handler();
        imgRun = new ImageSwitchRunnable(false, introImage, R.drawable.intro1p2, R.drawable.intro1p1, imgHandler);
        imgHandler.postDelayed(
                imgRun, 1000);
    }

    private void setPractice2() {
        int introRawId = getResources().getIdentifier(getPackageName() + ":raw/intro2s", null, null);
        Log.d(this.getClass().getName(), "id:" + getPackageName() + ":raw/intro2s");
        try {
            //media = MediaPlayer.create(this, raw1);
            mediaPlayer = MediaPlayer.create(this, introRawId);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.d(this.getClass().getName(), "Media completed" + ":raw/intro2s");
                    mp.stop();
                    Dialog dialog = new DynamicDialog(IntroActivity.this, "按下一步，開始練習題", "下一步");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finishIntro();
                        }
                    });
                    dialog.show();
                }
            });
            mediaPlayer.start();
            imgHandler2 = new Handler();
            imgHandler2.postDelayed(
                    new ImageSwitchRunnable(false, introImage, R.drawable.intro2p2, R.drawable.intro2p1, imgHandler2)  , 1000);
            imageHandler3 = new Handler();// for arrow hint
            imageHandler3.postDelayed(
                    new ImageSwitchRunnable(true, introArrow, imageHandler3), 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishIntro() {
        Intent intent = new Intent();
        intent.putExtras(bundle);
        intent.setClass(IntroActivity.this, ExperimentActivity.class);
        startActivity(intent);
        try {
            mediaPlayer.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.finish();
    }

    class NextStepDialog extends Dialog {

        public NextStepDialog(Context context) {
            super(context);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

    }
    protected boolean switchTemp = false;
    public class ImageSwitchRunnable implements Runnable {
        int img1, img2;
        boolean isSwitchVisable;
        ImageView iv;
        Handler handler;
        private ImageSwitchRunnable(boolean isSwitchVisable,ImageView iv, int img1, int img2, Handler handler){
            this.isSwitchVisable = isSwitchVisable;
            this.img1 = img1;
            this.img2 = img2;
            this.iv = iv;
            this.handler = handler;
        }
        private ImageSwitchRunnable(boolean isSwitchVisable,ImageView iv, Handler handler){
            this.isSwitchVisable = isSwitchVisable;
            this.img1 = img1;
            this.img2 = img2;
            this.iv = iv;
            this.handler = handler;
        }
        @Override
        public void run() {
            if(isSwitchVisable)
                switchVisable();
            else
                switchImgs();
        }
        private void setImgs(int img1, int img2){
            this.img1 = img1;
            this.img2 = img2;
        }
        private void switchVisable(){
            if (switchTemp) {
                iv.setVisibility(View.INVISIBLE);
                switchTemp = false;
                handler.postDelayed(this, 500);
            } else {
                iv.setVisibility(View.VISIBLE);
                switchTemp = true;
                handler.postDelayed(this, 500);
            }
        }
        private void switchImgs(){
            if(switchTemp){
                iv.setImageResource(img1);
                switchTemp=false;
                handler.postDelayed(this, 1000);
            }else{
                iv.setImageResource(img2);
                switchTemp=true;
                handler.postDelayed(this, 1000);
            }
        }
    }

}

