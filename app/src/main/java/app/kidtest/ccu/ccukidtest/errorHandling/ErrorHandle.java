package app.kidtest.ccu.ccukidtest.errorHandling;

import android.content.Context;
import android.widget.Toast;

import app.kidtest.ccu.ccukidtest.R;


/**
 * Created by kasonlu on 16/2/8.
 */
public class ErrorHandle {
    public static final int ERROR_FILE_NEW = 401;
    public static final int ERROR_INPUT_EMPTY = 301;
    public static final int ERROR_DB_QUERY = 501;
    public static final int ERROR_FILE_DATEFORMAT = 601;
    public static final int ERROR_FILE_NOTFOUND = 602;
    public static final int ERROR_FILE_DATAFORMAT = 603;
    public static void showErrorMessage(Context context, int errorCode, String msg){
        Toast.makeText(context, msg+ " code:"+errorCode, Toast.LENGTH_SHORT).show();
    }

    public static void showErrorMessage(Context context, int errorCode){
        String msg = "";
        switch(errorCode){
            case ERROR_FILE_NOTFOUND:
                msg = context.getResources().getString(R.string.experiment_file_notfound_error);
                break;
            case ERROR_FILE_DATEFORMAT:
                msg = context.getResources().getString(R.string.main_date_format_error);
                break;
            case ERROR_FILE_DATAFORMAT:
                msg = context.getResources().getString(R.string.main_data_format_error);
                break;
            default:
                break;
        }
        Toast.makeText(context, msg+ " code:"+errorCode, Toast.LENGTH_SHORT).show();
    }

}
