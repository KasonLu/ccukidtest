package app.kidtest.ccu.ccukidtest.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.kidtest.ccu.ccukidtest.Application.CCUTestApplication;
import app.kidtest.ccu.ccukidtest.R;
import app.kidtest.ccu.ccukidtest.errorHandling.ErrorHandle;
import app.kidtest.ccu.ccukidtest.object.Student;
import app.kidtest.ccu.ccukidtest.utility.LoadStudent;
import app.kidtest.ccu.ccukidtest.utility.MenuControl;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton profileImgBtnSubmit;
    TextView profileTvSubmit;
    EditText profileEdtvSchool, profileEdtvGrade, profileEdtvClass, profileEdtvName, profileEdtvNumber;
    Spinner profileSpLoad;
    DatePicker profileDpBirthday;
    RadioButton profileRbtnFemale, profileRbtnMale;
    RadioGroup profileRg;
    ArrayList<String> list = new ArrayList<String>();
    List<Student> studentList = null;
    MenuControl menuControl = new MenuControl(this);
    String[] permission_storage = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(canMakeSmores())
            permissionRequest();
        else {
            setup();
        }
    }

    protected void setup(){
        createDir();
        setContentView(R.layout.activity_main);
        setUI();
    }
    @TargetApi(Build.VERSION_CODES.M)
    protected void permissionRequest(){
        int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission_group.STORAGE);
        Log.d(this.getClass().getName(), "Permission check:"+permissionCheck);
        switch (permissionCheck){
            case 200:
                setup();
                break;
            default:
                requestPermissions(permission_storage, 200);
        }


    }
    private boolean canMakeSmores(){

        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    @Override

    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:
                setup();
                break;
            default :
                Toast.makeText(this, "請重新開啟App並同意權限", Toast.LENGTH_LONG).show();
                this.finish();

        }

    }
    protected void createDir(){
        File sdcard = Environment.getExternalStorageDirectory();
        File rootDir = new File(sdcard, CCUTestApplication.dirName);
        if(!rootDir.exists()) {
            rootDir.mkdirs();
        }
        File settingDir = new File(rootDir, CCUTestApplication.settingDir);
        if(!settingDir.exists())
            settingDir.mkdirs();

        File testDir = new File(rootDir, CCUTestApplication.testDirName);
        if(!testDir.exists())
            testDir.mkdirs();
        File outcomeDir = new File(rootDir, CCUTestApplication.outcomeDir);
        if(!outcomeDir.exists())
            outcomeDir.mkdirs();
    }
    private void setUI(){
        profileImgBtnSubmit = (ImageButton)findViewById(R.id.profile_imgBtn_submit);
        profileImgBtnSubmit.setOnClickListener(this);
        profileTvSubmit = (TextView)findViewById(R.id.profile_tv_submit);
        profileTvSubmit.setOnClickListener(this);
        profileEdtvSchool = (EditText)findViewById(R.id.profile_edtv_school);
        profileEdtvGrade = (EditText)findViewById(R.id.profile_edtv_grade);
        profileEdtvClass = (EditText)findViewById(R.id.profile_edtv_class);
        profileEdtvName = (EditText)findViewById(R.id.profile_edtv_name);
        profileEdtvNumber = (EditText)findViewById(R.id.profile_edtv_number);
        profileDpBirthday = (DatePicker)findViewById(R.id.profile_dp_birthday);
        profileRbtnFemale = (RadioButton)findViewById(R.id.profile_rb_female);
        profileRbtnMale = (RadioButton)findViewById(R.id.profile_rb_male);
        profileRbtnMale.setChecked(true);
        profileRg = (RadioGroup)findViewById(R.id.profile_rg);
        profileSpLoad = (Spinner)findViewById(R.id.profile_sp_load);
        if(CCUTestApplication.DEGUG_MODE){
            profileEdtvSchool.setText("測試國小");
            profileEdtvGrade.setText("1");
            profileEdtvClass.setText("放牛班");
            profileEdtvName.setText("路人");
            profileEdtvNumber.setText("74");
        }
        try {
            studentList = new LoadStudent().loadStudentFromExcel(MainActivity.this);
        }catch(Exception e){
            e.printStackTrace();
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_FILE_DATAFORMAT);
        }
        list.add(0, getResources().getString(R.string.profile_load_spiner));//default spiner text
        if(studentList!=null) {
            for (int i = 0; i < studentList.size(); i++) {
                String name = studentList.get(i).getStuName();
                if (!name.equals(""))
                    list.add(i+1, name);// i+1 because, 0 is for spiner's title use ( not student data)
            }
        }
        profileSpLoad.setAdapter(new ArrayAdapter<String>(this, R.layout.main_spiner_textview, list));
        profileSpLoad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    // do nothing ;
                }else {
                    Student student = studentList.get(position-1);// position is shift right with value 1 ( because 0 for title uses )
                    profileEdtvSchool.setText(student.getStuSchoolName());
                    profileEdtvGrade.setText(student.getStuGrade());
                    profileEdtvClass.setText(student.getStuClass());
                    profileEdtvName.setText(student.getStuName());
                    profileEdtvNumber.setText(student.getStuNumber());
                    if (student.getStuGender() == 0)
                        profileRbtnMale.setChecked(true);
                    else
                        profileRbtnFemale.setChecked(true);
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                    try {
                        calendar.setTime(format.parse(student.getStuBirthday()));
                        profileDpBirthday.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorHandle.showErrorMessage(MainActivity.this, ErrorHandle.ERROR_FILE_DATEFORMAT);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void startTest(Student student){
        Bundle bundle = new Bundle();
        bundle.putString(ExperimentActivity.BUNDLE_STUDENT, student.studentToJSON().toString());
        Intent intent = new Intent();
        intent.putExtras(bundle);
        intent.setClass(MainActivity.this, IntroActivity.class);
        startActivity(intent);
    }
    private Student checkInput(){
        String stuSchool=null;
        String stuGrade=null;
        String stuClass=null;
        int stuGender = -1;
        String stuName=null;
        String stuNumber=null;
        String stuBirthday=null;
        //School
        if(profileEdtvSchool.getText().length()<=0) {
        /*  comment because fast start the test without checking.
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_INPUT_EMPTY, this.getResources().getString(R.string.profile_school_error));
            return null; */
            stuSchool = "";
        }else
            stuSchool = profileEdtvSchool.getText().toString();
        //Grade
        if(profileEdtvGrade.getText().length()<=0) {
        /*  comment because fast start the test without checking.
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_INPUT_EMPTY, this.getResources().getString(R.string.profile_grade_error));
            return null; */
            stuGrade = "";
        }else
            stuGrade = profileEdtvGrade.getText().toString();
        //Class
        if(profileEdtvClass.getText().length()<=0) {
        /*  comment because fast start the test without checking.
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_INPUT_EMPTY, this.getResources().getString(R.string.profile_class_error));
            return null;*/
            stuClass = "";
        }else
            stuClass = profileEdtvClass.getText().toString();
        //Gender
        switch (profileRg.getCheckedRadioButtonId()){
            case R.id.profile_rb_male:
                stuGender=1;
                break;
            case R.id.profile_rb_female:
                stuGender=0;
                break;
        }
        //Name
        if(profileEdtvName.getText().length()<=0) {
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_INPUT_EMPTY, this.getResources().getString(R.string.profile_name_error));
            return null;
        }else
            stuName = profileEdtvName.getText().toString();
        //Number
        if(profileEdtvNumber.getText().length()<=0) {
        /*  comment because fast start the test without checking.
            ErrorHandle.showErrorMessage(this, ErrorHandle.ERROR_INPUT_EMPTY, this.getResources().getString(R.string.profile_number_error));
            return null;*/
            stuNumber = "";
        }else
            stuNumber = profileEdtvNumber.getText().toString();
        //Birthday
        Calendar calendar = Calendar.getInstance();
        calendar.set(profileDpBirthday.getYear(), profileDpBirthday.getMonth(), profileDpBirthday.getDayOfMonth());
        //stuBirthday = String.valueOf(calendar.getTimeInMillis());
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        stuBirthday = format.format(calendar.getTime());
        Student student = new Student(stuSchool, stuGrade, stuClass, stuGender, stuName, stuNumber, stuBirthday, new JSONArray(), 0, "", 0, 0);
        return student;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.profile_imgBtn_submit:
            case R.id.profile_tv_submit:
                Student student = checkInput();
                if(student!=null)
                    startTest(student);
                break;
        }
    }

    // Autocomplete TextView
    public class InstantAutoCompleteTextView extends AutoCompleteTextView {

        public InstantAutoCompleteTextView(Context context) {
            super(context);
        }

        public InstantAutoCompleteTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public InstantAutoCompleteTextView(Context context, AttributeSet attrs,
                                           int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        public boolean enoughToFilter() {
            return true;
        }

        @Override
        protected void onFocusChanged(boolean focused, int direction,
                                      Rect previouslyFocusedRect) {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
            if (focused && getAdapter() != null) {
                performFiltering(getText(), KeyEvent.KEYCODE_UNKNOWN);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.result_action_check_count:
                // User chose the "Settings" item, show the app settings UI...
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_check_count)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, getString(R.string.result_check_count), Toast.LENGTH_SHORT).show();
                                menuControl.showStudentCount();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            case R.id.result_action_generate_file:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_generate_file)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, getString(R.string.result_generate_file), Toast.LENGTH_SHORT).show();
                                menuControl.generateFile();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            case R.id.result_action_resetdb:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.result_alert_title)
                        .setMessage(R.string.result_reset_db)
                        .setPositiveButton(R.string.result_alert_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, getString(R.string.result_reset_db), Toast.LENGTH_SHORT).show();
                                menuControl.cleanDb();
                            }
                        })
                        .setNegativeButton(R.string.result_alert_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.result_items, menu);
        return true;
    }
}