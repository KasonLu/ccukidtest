package app.kidtest.ccu.ccukidtest.Application;

import android.app.Application;

/**
 * Created by kasonlu on 16/3/3.
 */
public class CCUTestApplication extends Application{
    final public static String testDirName = "TestData";//put the test img and mp3
    final public static String dirName ="NCYU-App";//Root forlder
    final public static String settingDir = "Setting";//put the setting file, ex:profile, question
    final public static String outcomeDir = "Outcome";//put the outcome excel files
    final public static String debugDir = "DebugLog";//put the debug log
    public static boolean DEGUG_MODE = false;//switch debug mode
    public static boolean TEST_SOURCE_APPEND = true;

}
