package app.kidtest.ccu.ccukidtest.utility;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.kidtest.ccu.ccukidtest.Application.CCUTestApplication;
import app.kidtest.ccu.ccukidtest.errorHandling.ErrorHandle;
import app.kidtest.ccu.ccukidtest.object.Student;
import jxl.Cell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

/**
 * Created by kasonlu on 16/3/29.
 */
public class LoadStudent {
    final public String TITLE_SCHOOL="校名";
    final public String TITLE_GRADE="年級";
    final public String TITLE_ClASS="班級";
    final public String TITLE_GENDER="性別";
    final public String TITLE_NAME="姓名";
    final public String TITLE_NUMBER="座號";
    final public String TITLE_BIRTHDAY="生日";
    final String loadFileName="StudentList.xls";
    private File rootDir;
    public  LoadStudent(){
        File sdcard = Environment.getExternalStorageDirectory();
        sdcard.setWritable(true);
        Log.d(this.getClass().getName(), "Storage State:" + Environment.getExternalStorageState());
        Log.d(this.getClass().getName(), "Storage:"+Environment.getExternalStorageDirectory());
        rootDir = new File(Environment.getExternalStorageDirectory()+File.separator +CCUTestApplication.dirName);
        if(!checkDir(rootDir)) {
            Log.d(getClass().getName(), "Mkdir result:"+rootDir.mkdirs());
        }
        Log.d(this.getClass().getName(), "RootPath:" + rootDir.getAbsolutePath());
    }

    public List<Student> loadStudentFromExcel(Context context) throws Exception{
        File settingDir = new File(rootDir, CCUTestApplication.settingDir);
        File file = new File(settingDir, loadFileName );
        if(!checkFile(file)){
            Log.i(this.getClass().getName(), "Load Student File doesn't exist");
            return null;
        }else{
            Workbook oldWorkbook = Workbook.getWorkbook(file);
            Sheet sheet = oldWorkbook.getSheet(0);
            int studentCount = getStudentCount(sheet);
            if(studentCount<0){
                ErrorHandle.showErrorMessage(context, ErrorHandle.ERROR_FILE_NEW, loadFileName + " 檔案錯誤");
                Log.d(this.getClass().getName(), "Question count: null");
                return null;
            }
            List<Student> stuList =  getAnswerFromExcel(studentCount, sheet, context);
            oldWorkbook.close();
            return stuList;
        }
    }

    private boolean checkDir(File dir){
        if(!dir.exists()){
            dir.mkdirs();
            return false;
        }else{
            return true;
        }
    }
    private boolean checkFile(File file){
        if (!file.exists()) {
            return false;
        }else{
            return true;
        }
    }

    private int getStudentCount(Sheet sheet){
        Cell studentCountCell = sheet.getCell(0, 0);
        int studentCount = 0;
        if (studentCountCell == null) {
            return -1;
        } else {
            if (!studentCountCell.getContents().equals("")) {
                NumberCell nc = (NumberCell) studentCountCell;
                studentCount = (int)nc.getValue();//get students count by excel's position(0,0)
                Log.d(this.getClass().getName(), "Student count:" + studentCount);
            } else {
                Log.d(this.getClass().getName(), "student count: empty");
                return -1;
            }
        }
        return studentCount;
    }

    private List<Student> getAnswerFromExcel(int studentCount, Sheet sheet, Context context){
        List<Student> studentList = new ArrayList<Student>();
        for(int index=0, row=1; index<studentCount; index++, row++) {
            try {
                int column = 1;
                Cell cellSchool = sheet.getCell(column, row);//School
                Cell cellGrade = sheet.getCell(column + 1, row);//School
                Cell cellClass = sheet.getCell(column + 2, row);//School
                Cell cellGender = sheet.getCell(column + 3, row);//School
                Cell cellName = sheet.getCell(column + 4, row);//School
                Cell cellNumber = sheet.getCell(column + 5, row);//School
                Cell cellBirthday = sheet.getCell(column + 6, row);//School
                String school = cellSchool.getContents();
                String grade = cellGrade.getContents();
                String className = cellClass.getContents();
                String stringGender = cellGender.getContents();
                String name = cellName.getContents();
                String number = cellNumber.getContents();
                String birthday = cellBirthday.getContents();
                birthday = birthday.replace(" ", "");
                //check value
                int gender;
                if (stringGender.equals("男")) {
                    gender = 0;
                }
                else if(stringGender.equals("女")){
                    gender = 1;
                }else{
                    ErrorHandle.showErrorMessage(context, ErrorHandle.ERROR_FILE_NEW, loadFileName + "第" + row + "行格式錯誤");
                    continue;
                }
                studentList.add(new Student(school, grade, className, gender, name, number, birthday));
            }catch(Exception e){
                e.printStackTrace();
                Log.d(this.getClass().getName(), "Error get answer excel:" + row);
                ErrorHandle.showErrorMessage(context, ErrorHandle.ERROR_FILE_NEW, loadFileName + "第" + row + "行格式錯誤");
            }
        }
        return studentList;
    }
}
