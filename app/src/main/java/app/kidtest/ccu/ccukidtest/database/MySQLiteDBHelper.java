package app.kidtest.ccu.ccukidtest.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by kasonlu on 16/2/9.
 */
public class MySQLiteDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="ccuTest.db";
    private static final int DATABASE_VERSION=1;
    public static final String TABLE_NAME="student";
    public static final String COLUMN_ID="_id";
    public static final String COLUMN_DATA="data";
    private static final String CREATE_TABLE="create table "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_DATA + " text not null);" ;
    public MySQLiteDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(MySQLiteDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
}
