package app.kidtest.ccu.ccukidtest.object;

import org.json.JSONObject;

/**
 * Created by kasonlu on 16/2/9.
 */
public class Test {
    public static final String JSON_QUESTION_ANSWER="questionNumber";
    public static final String JSON_ANSWER = "answer";
    public static final String JSON_IS_CORRECT="isCorrect";
    public static final String JSON_ANSWER_TIME="answerTime";
    private String questionNumber;
    private int answer;
    private boolean isCorrect;
    private long answerTime;
    public Test(String questionNumber, int answer, boolean isCorrect, long answerTime){
        this.setQuestionNumber(questionNumber);
        this.setAnswer(answer);
        this.setIsCorrect(isCorrect);
        this.setAnswerTime(answerTime);
    }
    public Test(JSONObject json){
        jsonToTest(json);
    }
    public String getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }
    public static JSONObject testToJSON(Test test){
        JSONObject testObj = new JSONObject();
        try {
            testObj.put(JSON_QUESTION_ANSWER, test.getQuestionNumber());
            testObj.put(JSON_ANSWER, test.getAnswer());
            testObj.put(JSON_IS_CORRECT, test.isCorrect());
            testObj.put(JSON_ANSWER_TIME, test.getAnswerTime());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return testObj;
    }
    public Test jsonToTest(JSONObject json) {
        try {
            this.setQuestionNumber(json.getString(JSON_QUESTION_ANSWER));
            this.setAnswer(json.getInt(JSON_ANSWER));
            this.setIsCorrect(json.getBoolean(JSON_IS_CORRECT));
            this.setAnswerTime(json.getLong(JSON_ANSWER_TIME));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return this;
    }
}
