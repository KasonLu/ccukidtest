package app.kidtest.ccu.ccukidtest.object;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.kidtest.ccu.ccukidtest.database.MySQLiteDBHelper;

/**
 * Created by kasonlu on 16/2/2.
 */
public class Student {
    private String stuSchoolName;
    private String stuGrade;
    private String stuClass;
    private int stuGender;// 1 is Male, 0 is Female;
    private String stuName;
    private String stuNumber;
    private String stuBirthday;
    private JSONArray stuTestResult;
    private long stuAvgAnswerTime;// average time of testing
    private String testDate; // date of testing
    private int correctCount; // correct number
    private int questionCount;// number of questions

    public static final String JSON_STUDENT="student";
    public static final String JSON_SchoolName="schoolName";
    public static final String JSON_Grade="grade";
    public static final String JSON_Class="class";
    public static final String JSON_Gender="gender";
    public static final String JSON_Name="name";
    public static final String JSON_Number="number";
    public static final String JSON_Birthday="birthday";
    public static final String JSON_TestResult="testResult";
    public static final String JSON_TEST_DATE="testDate";
    public static final String JSON_CORRECT_COUNT="correctCount";
    public static final String JSON_QUESTION_COUNT="questionCount";
    public static final String JSON_AVERAGE_ANSWER_TIME="averageAnswerTime";

    public Student(Cursor cursor){
        try {
            cursor.moveToFirst();
            String jsonStr = cursor.getString(cursor.getColumnIndex(MySQLiteDBHelper.COLUMN_DATA));
            JSONObject studentJson = new JSONObject(jsonStr);
            setStudent(JsonToStudent(studentJson));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public Student(JSONObject json) throws JSONException{
        this(
                json.getString(JSON_SchoolName),
                json.getString(JSON_Grade),
                json.getString(JSON_Class),
                json.getInt(JSON_Gender),
                json.getString(JSON_Name),
                json.getString(JSON_Number),
                json.getString(JSON_Birthday),
                json.getJSONArray(JSON_TestResult),
                json.getLong(JSON_AVERAGE_ANSWER_TIME),
                json.getString(JSON_TEST_DATE),
                json.getInt(JSON_QUESTION_COUNT),
                json.getInt(JSON_CORRECT_COUNT)
        );
    }
    public Student(String stuSchoolName, String stuGrade, String stuClass,
                    int stuGender, String stuName, String stuNumber, String stuBirthday){
        this.stuSchoolName = stuSchoolName;
        this.stuGrade = stuGrade;
        this.stuClass = stuClass;
        this.stuGender = stuGender;
        this.stuName = stuName;
        this.stuNumber = stuNumber;
        this.stuBirthday = stuBirthday;
    }
    public Student(String stuSchoolName, String stuGrade, String stuClass,
                   int stuGender, String stuName, String stuNumber, String stuBirthday, JSONArray stuTestResult,
                   long stuAvgAnswerTime, String testDate, int questionCount, int correctCount){
        this.stuSchoolName = stuSchoolName;
        this.stuGrade = stuGrade;
        this.stuClass = stuClass;
        this.stuGender = stuGender;
        this.stuName = stuName;
        this.stuNumber = stuNumber;
        this.stuBirthday = stuBirthday;
        this.stuTestResult = stuTestResult;
        this.setStuAvgAnswerTime(stuAvgAnswerTime);
        this.setTestDate(testDate);
        this.setQuestionCount(questionCount);
        this.setCorrectCount(correctCount);
    }
    public void setStudent(Student student){
        this.setStuSchoolName(student.getStuSchoolName());
        this.setStuGrade(student.getStuGrade());
        this.setStuClass(student.getStuClass());
        this.setStuGender(student.getStuGender());
        this.setStuName(student.getStuName());
        this.setStuNumber(student.getStuNumber());
        this.setStuBirthday(student.getStuBirthday());
        this.setStuTestResult(student.getStuTestResult());
        this.setStuAvgAnswerTime(student.getStuAvgAnswerTime());
        this.setTestDate(student.getTestDate());
        this.setQuestionCount(student.getQuestionCount());
        this.setCorrectCount(student.getCorrectCount());
    }
    public String getStuSchoolName() {
        return stuSchoolName;
    }

    public void setStuSchoolName(String stuSchoolName) {
        this.stuSchoolName = stuSchoolName;
    }

    public String getStuGrade() {
        return stuGrade;
    }

    public void setStuGrade(String stuGrade) {
        this.stuGrade = stuGrade;
    }

    public String getStuClass() {
        return stuClass;
    }

    public void setStuClass(String stuClass) {
        this.stuClass = stuClass;
    }

    public int getStuGender() {
        return stuGender;
    }

    public void setStuGender(int stuGender) {
        this.stuGender = stuGender;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuNumber() {
        return stuNumber;
    }

    public void setStuNumber(String stuNumber) {
        this.stuNumber = stuNumber;
    }

    public String getStuBirthday() {
        return stuBirthday;
    }

    public void setStuBirthday(String stuBirthday) {
        this.stuBirthday = stuBirthday;
    }

    public static String getJSON_TestResult() {
        return JSON_TestResult;
    }

    public JSONArray getStuTestResult() {
        return stuTestResult;
    }

    public void setStuTestResult(JSONArray stuTestResult) {
        this.stuTestResult = stuTestResult;
    }


    public long getStuAvgAnswerTime() {
        return stuAvgAnswerTime;
    }

    public void setStuAvgAnswerTime(long stuAvgAnswerTime) {
        this.stuAvgAnswerTime = stuAvgAnswerTime;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public int getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(int correctCount) {
        this.correctCount = correctCount;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public JSONObject studentToJSON(){
        Student student = this;
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(JSON_SchoolName, student.getStuSchoolName());
            jsonObj.put(JSON_Grade, student.getStuGrade());
            jsonObj.put(JSON_Class, student.getStuClass());
            jsonObj.put(JSON_Gender, student.getStuGender());
            jsonObj.put(JSON_Name, student.getStuName());
            jsonObj.put(JSON_Number, student.getStuNumber());
            jsonObj.put(JSON_Birthday, student.getStuBirthday());
            jsonObj.put(JSON_TestResult, student.getStuTestResult());
            jsonObj.put(JSON_AVERAGE_ANSWER_TIME, student.getStuAvgAnswerTime());
            jsonObj.put(JSON_TEST_DATE, student.getTestDate());
            jsonObj.put(JSON_QUESTION_COUNT, student.getQuestionCount());
            jsonObj.put(JSON_CORRECT_COUNT, student.getCorrectCount());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return jsonObj;
    }
    public Student JsonToStudent(JSONObject json){

        try{
            this.setStuSchoolName(json.getString(JSON_SchoolName));
            this.setStuGrade(json.getString(JSON_Grade));
            this.setStuClass(json.getString(JSON_Class));
            this.setStuGender(json.getInt(JSON_Gender));
            this.setStuName(json.getString(JSON_Name));
            this.setStuNumber(json.getString(JSON_Number));
            this.setStuBirthday(json.getString(JSON_Birthday));
            this.setStuTestResult(json.getJSONArray(JSON_TestResult));
            this.setStuAvgAnswerTime(json.getLong(JSON_AVERAGE_ANSWER_TIME));
            this.setTestDate(json.getString(JSON_TEST_DATE));
            this.setQuestionCount(json.getInt(JSON_QUESTION_COUNT));
            this.setCorrectCount(json.getInt(JSON_CORRECT_COUNT));

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return this;
    }

}
