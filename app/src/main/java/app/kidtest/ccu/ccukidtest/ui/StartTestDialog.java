package app.kidtest.ccu.ccukidtest.ui;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.Window;
import android.widget.ImageView;

import app.kidtest.ccu.ccukidtest.R;

/**
 * Created by kasonlu on 16/3/9.
 */
public class StartTestDialog extends Dialog implements MediaPlayer.OnCompletionListener{
        ImageView iv;
    MediaPlayer mediaPlayer;
    public StartTestDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_imagelayout);
        iv = (ImageView)findViewById(R.id.introDialog_iv);
        iv.setImageResource(R.drawable.intro4p1);
        int introRawId = context.getResources().getIdentifier(context.getPackageName() + ":raw/intro4s", null, null);
        try {
        //media = MediaPlayer.create(this, raw1);
        mediaPlayer = MediaPlayer.create(context, introRawId);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.start();
        }catch(Exception e){
        e.printStackTrace();
        }
        myHandler = new Handler();
        myHandler.postDelayed(mMyRunnable, 1000);
}
        Handler myHandler;
@Override
public void onCompletion(MediaPlayer mp) {
        mp.stop();
        mp.release();
        StartTestDialog.this.dismiss();
        }
        boolean switchBoolean= true;
private Runnable mMyRunnable = new Runnable() {
    @Override
        public void run() {
            //Change state here
            if(switchBoolean){
                iv.setImageResource(R.drawable.intro4p2);
                switchBoolean=false;
                myHandler.postDelayed(this, 1000);
            }else{
                iv.setImageResource(R.drawable.intro4p1);
                switchBoolean=true;
                myHandler.postDelayed(this, 1000);
            }
        }
    };
}
