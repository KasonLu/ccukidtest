package app.kidtest.ccu.ccukidtest.utility;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import app.kidtest.ccu.ccukidtest.Application.CCUTestApplication;
import app.kidtest.ccu.ccukidtest.errorHandling.ErrorHandle;
import app.kidtest.ccu.ccukidtest.object.Student;
import app.kidtest.ccu.ccukidtest.object.Test;
import jxl.Cell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Created by kasonlu on 16/1/24.
 */
public class ExcelGenerate {
        final public String TITLE_SCHOOL="校名";
        final public String TITLE_GRADE="年級";
        final public String TITLE_ClASS="班級";
        final public String TITLE_GENDER="性別";
        final public String TITLE_NAME="姓名";
        final public String TITLE_NUMBER="座號";
        final public String TITLE_BIRTHDAY="生日";
        final public String TITLE_POINT="原始分數";
        final public String TITLE_CORRECT_NUMBER="答對題數";
        final public String TITLE_TEST_DATE="測驗日期";
        final public String TITLE_AVG_TIME="平均時間(s)";
        final public String TITLE_QUESTION_NUMBER="題號";
        final public String TITLE_ANSWER="回答";
        final public String TITLE_ISCORRECT="對錯";
        final public String TITLE_ANSWER_TIME="時間(s)";
        final public String SHEET_FIRST="student";
        final public String SHEET_SECOND="result";

        //final String indroDirName = "IntroductionData";
        //final String praDirName = "PracticeData";
        final String testResultFileName ="Outcome";
        final String testResultFileType = ".xls";
        final String questionFileName = "question.xls";
        final public String SHEET_QUESTION="questions";
         final int[] answerArray=
             {1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
              1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
              1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
              1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
              1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
        };
        private File rootDir;
        //final int[] answerArray=
        //    {1, 2};
        final public int DEFALUT_QUESTION_NUMBER = answerArray.length;
        public  ExcelGenerate(){
            File sdcard = Environment.getExternalStorageDirectory();
            rootDir = new File(sdcard, CCUTestApplication.dirName);
            if(!checkDir(rootDir)) {
                rootDir.mkdirs();
            }
            Log.d(this.getClass().getName(), "RootPath:"+ rootDir.getAbsolutePath() );
        }
        public void testGenerateFile(Context context, List<Student> stulist) throws IOException{

            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("zh", "TW"));
            WritableWorkbook workbook;
            File dir = new File(rootDir, CCUTestApplication.outcomeDir);
            if(!checkDir(dir)) {
                dir.mkdirs();
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            String testDate = format.format(calendar.getTime());
            File file = new File(dir, testResultFileName+ testDate + testResultFileType);
            if(!checkFile(file)){
                file.createNewFile();
                Log.i("ExcelBasicForm", "File doesn't exist");
            }else{
                Log.i("ExcelBasicForm", "File exist");
            }
            if(file==null) {
                //ErrorHandle.showErrorMessage(context, ErrorHandle.ERROR_FILE_NEW);
            }else {
                try {
                    workbook = Workbook.createWorkbook(file, wbSettings);
                    WritableSheet sheet = workbook.createSheet(SHEET_FIRST, 0);
                    WritableSheet sheet2 = workbook.createSheet(SHEET_SECOND, 1);
                    createSheetStudent(sheet);
                    createSheetResult(sheet2);
                    //Cell stuCountCell = sheet.getCell(0, 0);
                    /*
                    double stuCount = 0;
                    if (stuCountCell == null) {
                        Log.d("is null", "Null is Cell");
                        sheet.addCell(new Number(0, 0, 0)); //column:0, row:0, value:0

                    } else {
                        if (!stuCountCell.getContents().equals("")) {
                            NumberCell nc = (NumberCell) stuCountCell;
                            stuCount = nc.getValue();//get students count by excel's position(0,0)
                            Log.d("is not null", "stuCount:" + stuCount + "-" + stuCountCell.getContents() + "type:" + stuCountCell.getType() + " format:" + stuCountCell.getCellFormat());
                            sheet.addCell(new Number(0, 0, stuCount++)); //column:0, row:0, value:0
                        } else {
                            sheet.addCell(new Number(0, 0, 0)); //column:0, row:0, value:0
                            Log.d("is not null", stuCountCell.getContents() + "type:" + stuCountCell.getType() + " format:" + stuCountCell.getCellFormat());
                        }
                    }*/
                    //sheet1 initial
                    sheet.addCell(new Number(0,0, stulist.size()));
                    sheet.addCell(new Label(1, 0, TITLE_SCHOOL));
                    sheet.addCell(new Label(2, 0, TITLE_GRADE));
                    sheet.addCell(new Label(3, 0, TITLE_ClASS));
                    sheet.addCell(new Label(4, 0, TITLE_GENDER));
                    sheet.addCell(new Label(5, 0, TITLE_NAME));
                    sheet.addCell(new Label(6, 0, TITLE_NUMBER));
                    sheet.addCell(new Label(7, 0, TITLE_BIRTHDAY));
                    sheet.addCell(new Label(8, 0, TITLE_POINT));
                    sheet.addCell(new Label(9, 0, TITLE_CORRECT_NUMBER));
                    sheet.addCell(new Label(10, 0, TITLE_TEST_DATE));
                    sheet.addCell(new Label(11, 0, TITLE_AVG_TIME));

                    //sheet2 initial
                    sheet2.addCell(new Number(0,0, stulist.size()));
                    sheet2.addCell(new Label(1, 0, TITLE_SCHOOL));
                    sheet2.addCell(new Label(2, 0, TITLE_GRADE));
                    sheet2.addCell(new Label(3, 0, TITLE_ClASS));
                    sheet2.addCell(new Label(4, 0, TITLE_GENDER));
                    sheet2.addCell(new Label(5, 0, TITLE_NAME));
                    sheet2.addCell(new Label(6, 0, TITLE_NUMBER));
                    sheet2.addCell(new Label(7, 0, TITLE_BIRTHDAY));
                    sheet2.addCell(new Label(8, 0, TITLE_QUESTION_NUMBER));
                    sheet2.addCell(new Label(9, 0, TITLE_ANSWER));
                    sheet2.addCell(new Label(10, 0, TITLE_ISCORRECT));
                    sheet2.addCell(new Label(11, 0, TITLE_ANSWER_TIME));

                    //insert students data
                    for(int i=0; i<stulist.size(); i++){
                        int row = i+1;
                        Student tempStudent = stulist.get(i);
                        String genderTemp = "男";
                        if(tempStudent.getStuGender()==0)
                            genderTemp = "女";
                        sheet.addCell(new Label(1,row, tempStudent.getStuSchoolName()));
                        sheet.addCell(new Label(2,row, tempStudent.getStuGrade()));
                        sheet.addCell(new Label(3,row, tempStudent.getStuClass()));
                        sheet.addCell(new Label(4,row, genderTemp));
                        sheet.addCell(new Label(5,row, tempStudent.getStuName()));
                        sheet.addCell(new Label(6,row, tempStudent.getStuNumber()));
                        sheet.addCell(new Label(7,row, tempStudent.getStuBirthday()));
                        sheet.addCell(new Label(8,row, String.valueOf(tempStudent.getCorrectCount()*2)));
                        sheet.addCell(new Label(9,row, String.valueOf(tempStudent.getCorrectCount())));
                        sheet.addCell(new Label(10,row, tempStudent.getTestDate()));
                        sheet.addCell(new Label(11,row, String.valueOf( (float)tempStudent.getStuAvgAnswerTime()/1000.0)) );
                    }
                    int sheet2row = 1;
                    for(int i=0; i<stulist.size(); i++){
                        Student tempStudent = stulist.get(i);
                        String genderTemp = "男";
                        if(tempStudent.getStuGender()==0)
                            genderTemp = "女";
                        JSONArray testjsonArray = tempStudent.getStuTestResult();
                        sheet2.addCell(new Label(1,sheet2row, tempStudent.getStuSchoolName()));
                        sheet2.addCell(new Label(2,sheet2row, tempStudent.getStuGrade()));
                        sheet2.addCell(new Label(3,sheet2row, tempStudent.getStuClass()));
                        sheet2.addCell(new Label(4,sheet2row, genderTemp));
                        sheet2.addCell(new Label(5,sheet2row, tempStudent.getStuName()));
                        sheet2.addCell(new Label(6, sheet2row, tempStudent.getStuNumber()));
                        sheet2.addCell(new Label(7, sheet2row, tempStudent.getStuBirthday()));
                        for(int j=0; j< testjsonArray.length(); j++){
                            try{
                                if(testjsonArray.get(j)!=null){
                                    JSONObject testJson = (JSONObject)testjsonArray.get(j);
                                    Test answerObj = new Test(testJson);
                                    sheet2.addCell(new Label(8,sheet2row, answerObj.getQuestionNumber()));
                                    sheet2.addCell(new Label(9,sheet2row, String.valueOf(answerObj.getAnswer())));
                                    sheet2.addCell(new Label(10,sheet2row, String.valueOf(answerObj.isCorrect()?1:0)));
                                    sheet2.addCell(new Label(11,sheet2row, String.valueOf(( (float) answerObj.getAnswerTime()/1000.0) )));
                                    sheet2row++;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }

                    }

                    workbook.write();
                    workbook.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorHandle.showErrorMessage(context, -1, "test error");
                }
            }

    }
    private void createSheetStudent(Sheet studentSheet){

    }
    private void createSheetResult(Sheet resultSheet){

    }
    private boolean checkDir(File dir){
        if(!dir.exists()){
            return false;
        }else{
            return true;
        }
    }
    private boolean checkFile(File file){
        if (!file.exists()) {
            return false;
        }else{
            return true;
        }
    }

    public int[] questionAnswerDetect(Context context) throws Exception{
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("zh", "TW"));
        WritableWorkbook workbook;

        File dir = new File(rootDir, CCUTestApplication.settingDir);
        if(!checkDir(dir)) {
            dir.mkdirs();
        }
        File file = new File(dir, questionFileName );
        if(!checkFile(file)){
                        file.createNewFile();
                        Log.i("ExcelBasicForm", "Question File doesn't exist");
                        workbook = Workbook.createWorkbook(file, wbSettings);
                        WritableSheet sheet = workbook.createSheet(SHEET_QUESTION, 0);
                        int questionCount = getQuestionCount(sheet, DEFALUT_QUESTION_NUMBER);
                        if(questionCount<0){
                            Log.d(this.getClass().getName(), "Question count: null");
                            sheet.addCell(new Number(0, 0, DEFALUT_QUESTION_NUMBER)); //column:0, row:0, value:54( 50 real questions)
                            questionCount = DEFALUT_QUESTION_NUMBER;
                        }
                        sheet.addCell(new Label(0, 1, "題目編號"));
                        sheet.addCell(new Label(1, 1, "答案"));

                        //gernerate question number to excel
                        for(int questionNumber=1, column=0,row=2; questionNumber<= questionCount; row++, questionNumber++) {
                            sheet.addCell(new Label(column, row, String.valueOf(questionNumber)));
                        }
                        for(int index=0, column=1,row=2; index< questionCount; row++, index++) {
                            sheet.addCell(new Label(column, row, String.valueOf(answerArray[index])));
                        }
                        workbook.write();
                        workbook.close();
                        return answerArray;
        }else{
                        Log.i("ExcelBasicForm", "Question File exist");
                        Workbook oldWorkbook = Workbook.getWorkbook(file);
                        Sheet sheet = oldWorkbook.getSheet(0);
                        int questionCount = getQuestionCount(sheet, DEFALUT_QUESTION_NUMBER);
                        if(questionCount<0){
                            ErrorHandle.showErrorMessage(context, ErrorHandle.ERROR_FILE_NEW, questionFileName +" 檔案錯誤");
                            Log.d(this.getClass().getName(), "Question count: null");
                            questionCount = DEFALUT_QUESTION_NUMBER;
                        }

                        int[] answerArray = getAnswerFromExcel(questionCount, sheet);
                        oldWorkbook.close();
                        return answerArray;
        }

    }

    private int[] getAnswerFromExcel(int questionCount, Sheet sheet){
        int[] answerIntArray = new int[questionCount];
        for(int index=0, column=1, row=2; index<questionCount; index++, row++) {
            try {
                Cell cell = sheet.getCell(column, row);
                answerIntArray[index] = Integer.valueOf(cell.getContents());
            }catch(Exception e){
                e.printStackTrace();
                Log.d(this.getClass().getName(), "Error get answer excel:" + column + ":" + row);
            }
        }
        return answerIntArray;
    }
    // return -1 if no value;
    private int getQuestionCount(Sheet sheet, int defaultQuestionNumber){
        Cell questionCountCell = sheet.getCell(0, 0);
        int questionCount = 0;
        if (questionCountCell == null) {
            return -1;

        } else {
            if (!questionCountCell.getContents().equals("")) {
                NumberCell nc = (NumberCell) questionCountCell;
                questionCount = (int)nc.getValue();//get students count by excel's position(0,0)
                Log.d(this.getClass().getName(), "Question count:" + questionCount);
            } else {
                Log.d(this.getClass().getName(), "Question count: empty");
                return -1;
            }
        }
        return questionCount;
    }
}
