package app.kidtest.ccu.ccukidtest.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.kidtest.ccu.ccukidtest.object.Student;

/**
 * Created by kasonlu on 16/2/9.
 */
public class SQLiteDBControl {
    private SQLiteDatabase database;
    private SQLiteOpenHelper dbHelper;
    public static SQLiteDBControl instance = null;
    public String[] allColumns={
        MySQLiteDBHelper.COLUMN_ID, MySQLiteDBHelper.COLUMN_DATA
    };
    private SQLiteDBControl(Context context){
        dbHelper = new MySQLiteDBHelper(context);
    }
    synchronized static public SQLiteDBControl getInstance(Context context){
        if(instance==null)
            instance = new SQLiteDBControl(context);
        return instance;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }
    public Student addStudent(Student student){
        ContentValues values = new ContentValues();
        values.put(MySQLiteDBHelper.COLUMN_DATA, student.studentToJSON().toString());
        long inserId = database.insert(MySQLiteDBHelper.TABLE_NAME, null, values);
        Cursor cursor = database.query(MySQLiteDBHelper.TABLE_NAME, allColumns,
                MySQLiteDBHelper.COLUMN_ID + "=" + inserId, null, null, null, null);
        Student insertStudent = new Student(cursor);
        cursor.close();
        return insertStudent;
    }
    public List<Student> queryStudent(){
        this.open();
        Cursor cursor = database.rawQuery("select * from "+ MySQLiteDBHelper.TABLE_NAME, null);
        cursor.moveToFirst();
        if(cursor.getCount()>0){
            Log.d(this.getClass().getName(), "Count:" +cursor.getCount());
            List<Student> stuList = new ArrayList<Student>();
            for(int i=0;i<cursor.getCount();i++){
                try {
                    Student student = new Student(new JSONObject(cursor.getString(
                            cursor.getColumnIndex(MySQLiteDBHelper.COLUMN_DATA))));
                    stuList.add(student);
                    cursor.moveToNext();
                }catch(JSONException e){
                    e.printStackTrace();
                    break;
                }
            }
            cursor.close();
            this.close();
            return stuList;
        }
        this.close();
        return null;
    }
    public void cleanDb(){
        this.open();
        database.delete(MySQLiteDBHelper.TABLE_NAME, null, null);
        this.close();
    }
}
