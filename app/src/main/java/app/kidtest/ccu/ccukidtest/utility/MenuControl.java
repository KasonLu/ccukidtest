package app.kidtest.ccu.ccukidtest.utility;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;

import app.kidtest.ccu.ccukidtest.database.SQLiteDBControl;
import app.kidtest.ccu.ccukidtest.object.Student;

/**
 * Created by kasonlu on 16/4/2.
 */
public class MenuControl {
    Context context;
    public MenuControl(Context context){
        this.context = context;
    }

    public void generateFile(){
        try {
            List<Student> list = SQLiteDBControl.getInstance(context).queryStudent();
            if(list!=null) {
                new AsyncTask<List<Student>, Void, Void>() {
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        Toast.makeText(context, "產生成功！！！", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    protected void onPreExecute() {
                        Toast.makeText(context, "產生測驗檔案中....", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    protected Void doInBackground(List<Student>... params) {

                        ExcelGenerate excelGenerateTest = new ExcelGenerate();
                        try {
                            excelGenerateTest.testGenerateFile(context, params[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        }
                        return null;
                    }
                }.execute(list);
            }else{
                Toast.makeText(context, "Total: 0", Toast.LENGTH_LONG).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void cleanDb(){
        try {
            SQLiteDBControl.getInstance(context).cleanDb();
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void showStudentCount(){
        try {
            List<Student> list = SQLiteDBControl.getInstance(context).queryStudent();
            if(list!=null)
                Toast.makeText(context, "Total:" + list.size(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, "Total: 0", Toast.LENGTH_LONG).show();
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    }
}
